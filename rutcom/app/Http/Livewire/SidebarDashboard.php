<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SidebarDashboard extends Component
{
    public function render()
    {
        return view('livewire.sidebar-dashboard');
    }
}
