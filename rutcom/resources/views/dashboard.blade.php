<x-app-layout>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Dashboard') }}
        </h2>

    </x-slot>

        <livewire:sidebar-dashboard>
            <livewire:users-charts>
</x-app-layout>
